<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Customer;

use DB;

class DemoTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demo:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $customers = Customer::all();
        $id = 0;
        $errors = 0; 

        foreach($customers as $customer){

            try {
                
                DB::connection('dlds')->update('update r3pa_customer set website = "'.$customer->codigo.'" where siret = "'.$customer->rut.'"');
                $i++;

            } catch (\Throwable $th) {
                //throw $th;

                $errors++;
            }

        }


        echo "Se actualizaron ".$i." elementos. ".$errors." dieron error. ";

    }
}
