<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\CustomersImport;
use App\Models\Customer;

use DB;

class LoadExcelDataController extends Controller
{
    public function index(){


        $customers = Customer::all();

        foreach($customers as $key => $customer){

           echo "ID: ".$customer->id;
           echo "<br>Codigo: ".$customer->codigo;
           echo "<br>RUT: ".$customer->rut;
           echo "<br>Nombre: ".$customer->cliente;
           echo "<br>------------------------------------------<br>";
        }
    }


    public function find_and_replace(){

        //Get all customers

        $customers = Customer::all();
        $id = 0;
        $errors = 0; 

        foreach($customers as $customer){

            try {
                
                DB::connection('dlds')->update('update r3pa_customer set website = "'.$customer->codigo.'" where siret = "'.$customer->rut.'"');
                $i++;

            } catch (\Throwable $th) {
                //throw $th;

                $errors++;
            }

        }


        echo "Se actualizaron ".$i." elementos. ".$errors." dieron error. ";

    }


    public function test(){


       $customers =  DB::connection('dlds')->select('select * r3pa_customer ');

        dd($customers);


    }
}
